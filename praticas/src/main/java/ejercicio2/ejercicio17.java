/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;

import java.util.Scanner;

/**
 *
 * @author Draven
 */
public class ejercicio17 {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        // TODO code application logic here
  
        Scanner sc = new Scanner(System.in);
        //declarando la variable numero
        int numero=0;
        do{
            //pidiendo los dados por teclado y almacenandolo en numero
            System.out.println("Introduce un numero");
            numero=sc.nextInt();
            
           
        }while(numero<0); 
        int contador=0; 
        for (int i=numero;i>0;i/=10){
            //Incrementamos el contador
            contador++;
        }
  
        //Controlamos en el caso de que haya una cifra o mas
        if (contador==1){
            System.out.println("El numero "+numero+ " tiene "+contador+" cifra");
        }else{
            System.out.println("El numero "+numero+ " tiene "+contador+" cifras");
        }
  
    
    }
}
