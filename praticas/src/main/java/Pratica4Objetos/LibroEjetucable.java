/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica4Objetos;

/**
 *
 * @author Draven
 */
public class LibroEjetucable {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        // TODO code application logic here
        
        //Creamos lo objetos
        Libro libro1=new Libro(1111111111, "La bella y la bestia", "juan santana", 30);
        Libro libro2=new Libro(1111111112, "Como ser mejor persona", "Ronny De la cruz", 60);
         
        //Mostramos su estado
        System.out.println(libro1.toString());
        System.out.println(libro2.toString());
         
        //Modificamos el atributo numPaginas del libro1
        libro1.setNumPaginas(70);
         
        //Comparamos quien tiene mas paginas
        if(libro1.getNumPaginas()>libro2.getNumPaginas()){
            System.out.println(libro1.getTitulo()+" tiene más páginas");
        }else{
            System.out.println(libro2.getTitulo()+" tiene más páginas");
        }
         
         
    
    }
}
