/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica4Objetos;

/**
 *
 * @author Draven
 */
public class Persona {
    
   
    private final static char SEXO_DEF = 'H';
    
    public static final int INFRAPESO = -1;
 
   
    public static final int PESO_IDEAL = 0;
 
    
    public static final int SOBREPESO = 1;
    
    private String nombre;
 
    
    private int edad;
 
    
    private String cedula;
 
    
    private char sexo;
 
   
    private double peso;
 
    
    private double altura;
    
   
     
   public Persona() {
        this("", 0, SEXO_DEF, 0, 0);
    }
   public Persona(String nombre, int edad, char sexo) {
        this(nombre, edad, sexo, 0, 0);
    }
   public Persona(String nombre, int edad, char sexo, double peso, double altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.peso = peso;
        this.altura = altura;
        Gencedula();
        this.sexo = sexo;
        comprobarSexo();
    }
    private void comprobarSexo() {
 
        //Si el sexo no es una H o una M, por defecto es H
        if (sexo != 'H' && sexo != 'M') {
            this.sexo = SEXO_DEF;
        }
    }
    private void Gencedula() {
       
        java.util.Random cedu = new java.util.Random();
        
        String numcedula = "";
        
        for(int i = 0;i < 11; i++){
            numcedula += cedu.nextInt(10);
            if(i == 2 || i == 9){
                numcedula += "-";
            }
        }
        cedula = numcedula;
        System.out.println(cedula);
       
       
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setEdad(int edad) {
        this.edad = edad;
    }
    public void setSexo(char sexo) {
        this.sexo = sexo;
    }
    public void setPeso(double peso) {
        this.peso = peso;
    }
     public void setAltura(double altura) {
        this.altura = altura;
    }
     
     public int calcularIMC() {
        //Calculamos el peso de la persona
        double pesoActual = peso / (Math.pow(altura, 2));
        //Segun el peso, devuelve un codigo
        if (pesoActual >= 20 && pesoActual <= 25) {
            return PESO_IDEAL;
        } else if (pesoActual < 20) {
            return INFRAPESO;
        } else {
            return SOBREPESO;
        }
    }
     public boolean esMayorDeEdad() {
        boolean mayor = false;
        if (edad >= 18) {
            mayor = true;
        }
        return mayor;
    }
     @Override
    public String toString() {
        String exo;
        if (this.sexo == 'H') {
            exo = "hombre";
        } else {
            exo = "mujer";
        }
        return "Informacion de la persona:\n"
                + "Nombre: " + nombre + "\n"
                + "Sexo: " + sexo + "\n"
                + "Edad: " + edad + " años\n"
                + "Cedula: " + cedula + "\n"
                + "Peso: " + peso + " kg\n"
                + "Altura: " + altura + " metros\n";
    }
    
}
