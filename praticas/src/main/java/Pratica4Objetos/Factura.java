/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica4Objetos;

import java.util.ArrayList;

public class Factura {
    private final Double ITBIS = 0.18;
    private int NumeroFactura;
    private String FechaFactura;
    private Double TotalAPagar;
    private Double MontoConITBIS;
    private Double MontoConDescuento;
    private Double Monto;
    private Double Descuento;
    private ArrayList<Libro> libros;

    public Factura() {
        NumeroFactura = new java.util.Random().nextInt(10000);
        FechaFactura = "";
        TotalAPagar = Double.NaN;
        MontoConITBIS = Double.NaN;
        MontoConDescuento = Double.NaN;
        Monto = Double.NaN;
        Descuento = Double.NaN;
        libros = new ArrayList<Libro>();
    }

    public Factura(Double Descuento, ArrayList<Libro> libros) {
        this();
        java.util.Date date = new java.util.Date();
        this.Descuento = Descuento;
        this.libros = libros;
        calcularMonto();
        FechaFactura = String.format(
            "%s",
            date.toString()
        );
    }
    
    private void calcularMonto(){
        Monto = 0d;
        for(int i=0; i<libros.size(); i++){
            Monto += libros.get(i).getPrecio();
        }
        MontoConDescuento = descuento();
    }
    
    private Double getITBIS(){
        calcularMonto();
        Double itbis = Monto * ITBIS;
        MontoConITBIS = Monto + itbis;
        TotalAPagar = MontoConDescuento + itbis;
        return itbis;
    }
    
    @Override
    public String toString(){
        String lbs = "";
        for(int i=0; i<libros.size(); i++){
            lbs += "\t- " + libros.get(i).toString() + "\n";
        }
        return String.format(
            "-- Factura --"
            + "\nCODIGO DE FACTURA: %s"
            + "\nFECHA: %s"
            + "\nLIBROS:\n%s"
            + "MONTO: RD$ %s"
            + "\nDESCUENTO: RD$ %s"
            + "\nMONTO CON DESCUENTO: RD$ %s"
            + "\nTASA DE ITBIS: %s porciento"
            + "\nITBIS: RD$ %s"
            + "\nMONTO CON ITBIS: RD$ %s"
            + "\nTOTAL A PARGAR: RD$ %s",
            NumeroFactura,
            FechaFactura,
            lbs,
            Monto,
            Descuento,
            Math.round(MontoConDescuento),
            (ITBIS*100),
            Math.round(getITBIS()),
            Math.round(MontoConITBIS),
            Math.round(TotalAPagar)
        );
    }
    
    private Double descuento(){
        return Monto - Descuento;
    }

    public void setNumeroFactura(int NumeroFactura) {
        this.NumeroFactura = NumeroFactura;
    }

    public void setFechaFactura(String FechaFactura) {
        this.FechaFactura = FechaFactura;
    }

    public void setTotalAPagar(Double TotalAPagar) {
        this.TotalAPagar = TotalAPagar;
    }

    public void setMontoConITBIS(Double MontoConITBIS) {
        this.MontoConITBIS = MontoConITBIS;
    }

    public void setMontoConDescuento(Double MontoConDescuento) {
        this.MontoConDescuento = MontoConDescuento;
    }

    public void setLibros(ArrayList<Libro> libros) {
        this.libros = libros;
    }

    public int getNumeroFactura() {
        return NumeroFactura;
    }

    public String getFechaFactura() {
        return FechaFactura;
    }

    public Double getTotalAPagar() {
        return TotalAPagar;
    }

    public Double getMontoConITBIS() {
        return MontoConITBIS;
    }

    public Double getMontoConDescuento() {
        return MontoConDescuento;
    }

    public Double getMonto() {
        return Monto;
    }

    public Double getDescuento() {
        return Descuento;
    }
}