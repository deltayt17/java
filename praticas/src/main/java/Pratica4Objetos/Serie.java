/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica4Objetos;

/**
 *
 * @author Draven
 */
public class Serie {
    
     private final static int NUM_TEMPORADAS_DEF=3;
  
    
    public final static int MAYOR=1;
  
    
    public final static int MENOR=-1;
  
    
    public final static int IGUAL=0;
  
    
    private String titulo;
  
    
    private int numeroTemporadas;
  
    
    private boolean entregado;
  
   
    private String genero;
  
    private String creador;
    
     public String getTitulo() {
        return titulo;
    }
  
    /**
     * Modifica el titulo de la serie
     * @param titulo a cambiar
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
  
    /**
     * Devuelve la numeroTemporadas de la serie
     * @return numeroTemporadas de la serie
     */
    public int getnumeroTemporadas() {
        return numeroTemporadas;
    }
  
    /**
     * Modifica la numeroTemporadas de la serie
     * @param numeroTemporadas a cambiar
     */
    public void setnumeroTemporadas(int numeroTemporadas) {
        this.numeroTemporadas = numeroTemporadas;
    }
  
    /**
     * Devuelve el genero de la serie
     * @return genero de la serie
     */
    public String getGenero() {
        return genero;
    }
  
    /**
     * Modifica el genero de la serie
     * @param genero a cambiar
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }
  
    /**
     * Devuelve el creador de la serie
     * @return creador de la serie
     */
    public String getcreador() {
        return creador;
    }
    public void setcreador(String creador) {
        this.creador = creador;
    }
  
    /**
     * Cambia el estado de entregado a true
     */
    public void entregar() {
        entregado=true;
    }
  
    /**
     * Cambia el estado de entregado a false
     */
    public void devolver() {
        entregado=false;
    }
  
    /**
     * Indica el estado de entregado
     */
    public boolean isEntregado() {
        if(entregado){
            return true;
        }
        return false;
    }
    public int compareTo(Object a) {
        int estado=MENOR;
  
        //Hacemos un casting de objetos para usar el metodo get
        Serie ref=(Serie)a;
        if (numeroTemporadas>ref.getnumeroTemporadas()){
            estado=MAYOR;
        }else if(numeroTemporadas==ref.getnumeroTemporadas()){
            estado=IGUAL;
        }
  
        return estado;
    }
    
    public Serie(){
        this("",NUM_TEMPORADAS_DEF, "", "");
    }
  
    
    public Serie(String titulo, String creador){
        this(titulo,NUM_TEMPORADAS_DEF, "Accion", creador);
    }
  
    
    public Serie(String titulo, int numeroTemporadas, String genero, String creador){
        this.titulo=titulo;
        this.numeroTemporadas=numeroTemporadas;
        this.genero=genero;
        this.creador=creador;
        this.entregado=false;
    }
    public String toString(){
        return "Informacion de la Serie: \n" +
                "\tTitulo: "+titulo+"\n" +
                "\tNumero de temporadas: "+numeroTemporadas+"\n" +
                "\tGenero: "+genero+"\n" +
                "\tCreador: "+creador;
    }
}
