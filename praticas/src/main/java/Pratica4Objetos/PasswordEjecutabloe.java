/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica4Objetos;

import java.util.Scanner;

/**
 *
 * @author Draven
 */
public class PasswordEjecutabloe {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        // TODO code application logic hereScanner in = new Scanner(System.in);
        Scanner in = new Scanner(System.in);
        System.out.print("Introduce un tamaño para el array:");
        int tamanio = in.nextInt();
        
        System.out.print("Introduce la longitud del password:");
        int longitud = in.nextInt();
        
        Password listaPassword[]=new Password[tamanio];
        boolean fortalezaPassword[]=new boolean[tamanio];
        
        for(int i=0;i<listaPassword.length;i++){
            listaPassword[i]=new Password(longitud);
            fortalezaPassword[i]=listaPassword[i].esFuerte();
            System.out.println(listaPassword[i].getContraseña()+" "+fortalezaPassword[i]);
        }
    }
}
