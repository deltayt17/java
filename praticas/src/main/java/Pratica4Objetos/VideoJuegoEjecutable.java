/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica4Objetos;

/**
 *
 * @author Draven
 */
public class VideoJuegoEjecutable {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        // TODO code application logic here
        VideoJuego listVideoJuego[] = new VideoJuego[3];
        
        listVideoJuego[0]=new VideoJuego();
        listVideoJuego[1]=new VideoJuego("Assasin creed Syndicate", 30, "Aventura", "EA");
        listVideoJuego[2]=new VideoJuego("God of war Ascencion", "Santa Monica");
        
        for (int i = 0; i < listVideoJuego.length; i++ ){
            listVideoJuego[i].entregar();
            listVideoJuego[i].devolver();
            System.out.println(listVideoJuego[i]);
        }
    }
}
