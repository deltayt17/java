/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica4Objetos;

/**
 *
 * @author Draven
 */
public class Cuenta {
    
    private String titular;
    private double cantidad;
    
    public Cuenta(String titular){
       this(titular, 0);
    }
    
    public Cuenta(String titular, double cantidad){
        this.titular = titular;
        if (cantidad < 0){
            this.cantidad = 0;
        }else{
            this.cantidad = cantidad;
        }
    }
    //metodos set y get de los atributos
    public String Gettitular() {
        return titular;
    }
 
    public void Settitular (String titular) {
        this.titular = titular;
    }
 
    public double Getcantidad() {
        return cantidad;
    }
 
    public void SetCantidad(double cantidad) {
        this.cantidad = cantidad;
    }
    // metodo ingresar y retirar 
    public void ingresar (double cantidad){
        if(cantidad > 0){
            this.cantidad += cantidad;
        }
    }
    
    public void retirar (double cantidad){
        if (this.cantidad - cantidad < 0) {
            this.cantidad = 0;
        } else {
            this.cantidad -= cantidad;
        }
    }
    @Override
    public String toString() {
        return "El titular " + titular + " tiene " + cantidad + " pesos en la cuenta";
    }
    
}   
