/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica4Objetos;

/**
 *
 * @author Draven
 */
public class CuentaApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        // TODO code application logic here
       // Cuenta cuenta1 = new Cuenta("juan");
        Cuenta cuenta2 = new Cuenta("Ronny", 1000);
        
        //ingresar
        cuenta2.ingresar(2000);
                
        //retirar
        cuenta2.retirar(500);
        
        System.out.println(cuenta2.toString());
    }
}
