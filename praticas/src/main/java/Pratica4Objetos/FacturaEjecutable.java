/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica4Objetos;

/**
 *
 * @author Draven
 */
public class FacturaEjecutable {

   private static void mostrarLibroMasCaro(Libro lb, Libro lb1){
        System.out.println(
            String.format(
                "El libro '%s' es mas caro que '%s', con un precio de RD$ %s", 
                lb.getTitulo(),
                lb1.getTitulo(),
                lb.getPrecio()
            )
        );
    }
    
    public static void main(String[] args){
        Libro libro = new Libro();
        
        libro.setISBN("15768-45279-425");
        libro.setTitulo("Harry Potter");
        libro.setAuthor("Mary GrandPré");
        libro.setNumeroPagina(3665);
        libro.setPrecio(3322.93);
        
        Libro libro1 = new Libro();
        
        libro1.setISBN("97480-68744-547");
        libro1.setTitulo("El Señor de los Anillos");
        libro1.setAuthor("John Ronald Reuel Tolkien");
        libro1.setNumeroPagina(1368);
        libro1.setPrecio(817.30);
        
        System.out.println("------------------------------");
        System.out.println("Libro #1");
        System.out.println(libro.toString());
        System.out.println("------------------------------");
        System.out.println("Libro #2");
        System.out.println(libro1.toString());
        System.out.println("------------------------------");
        
        if(libro.getPrecio() > libro1.getPrecio()){
            mostrarLibroMasCaro(libro, libro1);
        }else{
            mostrarLibroMasCaro(libro1, libro);
        }
        
        // Agregando libros
        java.util.ArrayList<Libro> libros = new java.util.ArrayList<Libro>();
        libros.add(libro);
        libros.add(libro1);
        libros.add(libro1);
        
        // Creando factura
        Factura factura = new Factura(150.45, libros);
        
        // Imprimiendo factura
        System.out.println("\n**************\n" + factura.toString());
    }

}
