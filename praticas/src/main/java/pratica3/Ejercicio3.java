/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pratica3;

import java.util.Scanner;

/**
 *
 * @author Draven
 */
public class Ejercicio3 {

    Scanner dato = new Scanner(System.in);

    public void multiplicar(int n1) {

        System.out.println("Ingrese el numero que quiere multiplicar");
        n1 = dato.nextInt();
        for (int a = 1; a <= 12; a++) {
            int resul = a * n1;
            System.out.println( a + " x " + n1 + " = " + resul);
        }
    }

    public static void main(String[] args) {
        Ejercicio3 tabla = new Ejercicio3();
        tabla.multiplicar(0);
    }

}
