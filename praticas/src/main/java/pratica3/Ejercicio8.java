/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pratica3;

import java.util.Scanner;

/**
 *
 * @author Draven
 */
public class Ejercicio8 {

    Scanner dato = new Scanner(System.in);

    public void dobleotriple() {
        int numero;
        System.out.println("Ingrese el numero a evaluar ");
        numero = dato.nextInt();
        if (numero % 2 == 0) {
            System.out.println("El doble del numero que puso es " + 2 * numero);
        } else {
            System.out.println("El triple del numero que puso es  " + 3 * numero);
        }
    }

    public static void main(String[] args) {
        Ejercicio8 dt = new Ejercicio8();
        dt.dobleotriple();
    }
}
