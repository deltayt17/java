/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pratica3;

import java.util.Scanner;

/**
 *
 * @author Draven
 */
public class Ejercicio7 {

    public void par(){
        Scanner pedir = new Scanner(System.in);
        System.out.println("Introduce el numero y mostrara los par del 2 al que ponga");
        int p = pedir.nextInt();
        for(int i = 2; i <= p; i++){
            if ((i % 2) == 0){
                System.out.println(i + " es par");
            }
        }
    
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        // TODO code application logic here
        Ejercicio7 l = new Ejercicio7();
        l.par();
    }
}
