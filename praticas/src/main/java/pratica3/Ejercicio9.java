/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pratica3;

import java.util.Scanner;
/**
 *
 * @author Draven
 */
public class Ejercicio9 {

   

    public void cifras() {
        Scanner dato = new Scanner(System.in);
        int numero;
        System.out.println("Ingrese un numero");
        numero = dato.nextInt();
        if (numero >= 1 && numero < 10) {
            System.out.println("Ha ingresado un numero de un digito ");
        } else if (numero >= 10 && numero < 100) {
            System.out.println("Ha ingresado un numero de dos digito ");
        } else {
            System.out.println("Ha ingresado un numero de tres digito ");
        }
    }

    public static void main(String[] args) {
        Ejercicio9 cif = new Ejercicio9();
        cif.cifras();
    }
}
