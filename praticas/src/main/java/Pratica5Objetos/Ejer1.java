/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author dheid
 */
public class Ejer1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Empleado E1 = new Empleado("heidy");
        Directivo D1 = new Directivo("eliana");
        Operario O1 = new Operario("juan");
        Oficial OF1 = new Oficial("ronny");
        Tecnico T1 = new Tecnico("antony");
        
        System.out.println(E1);
        System.out.println(D1);
        System.out.println(O1);
        System.out.println(OF1);
        System.out.println(T1);
        
    }
    
}
