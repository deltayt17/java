/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author dheid
 */
public class Tecnico {
     Operario operario1 = new Operario();
    
    public Tecnico(){
        this.operario1.empleado2.setNombre("");
    }

    public Tecnico(String nombre  ) {
        this.operario1.empleado2.setNombre(nombre);
    }
    
    @Override
    public String toString(){
        return this.operario1.empleado2.toString()+" -> Operario -> Tecnico";
    }
}
