/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author Draven
 */
public class Coche extends Vehichulo{
    public double velocidad;
    
    public double cilindrada;

    public Coche(double velocidad, double cilindrada, String color, int ruedas) {
        super(color, ruedas);
        this.velocidad = velocidad;
        this.cilindrada = cilindrada;
    }

    public double getVelocidad() {
        return velocidad;
    }

    public double getCilindrada() {
        return cilindrada;
    }
    
    
    
    
}
