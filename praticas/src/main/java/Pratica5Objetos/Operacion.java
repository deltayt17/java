/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author Draven
 */
public class Operacion {
    //atributos
    public double numero1;
    public double numero2;
    public char operaciones;
    public double resultado;

    //constructor
    public Operacion(double numero1, double numero2, char operaciones, double resultado) {
        this.numero1 = numero1;
        this.numero2 = numero2;
        this.operaciones = operaciones;
        this.resultado = resultado;
    }
    public void mostrarResultado(){
        switch(operaciones){
            case '+':
                this.resultado = this.numero1 + this.numero2;
                break;
            case '-':
                this.resultado = this.numero1 - this.numero2;
                break;
            case '*':
                this.resultado = this.numero1 * this.numero2;
                break; 
            case '/':
                this.resultado = this.numero1 /this.numero2;
                break;    
            default:
                break;
        }
    }

    public double getResultado() {
        return resultado;
    }   
}
