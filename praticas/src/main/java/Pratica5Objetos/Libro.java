/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author Draven
 */
public class Libro extends Publicacion{
    
    public int numerospaginas;

    public Libro(int numerospaginas, String titulo, double precio) {
        super(titulo, precio);
        this.numerospaginas = numerospaginas;
    }

    public int getNumerospaginas() {
        return numerospaginas;
    }
      
}
