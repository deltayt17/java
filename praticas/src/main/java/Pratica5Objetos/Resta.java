/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author Draven
 */
public class Resta extends Operacion{
    public double resta;

    public Resta( double numero1, double numero2, char operaciones, double resultado) {
        super(numero1, numero2, '-', resultado);
        this.resta = getResultado();
    } 
    
}
