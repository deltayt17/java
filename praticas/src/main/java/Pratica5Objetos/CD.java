/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author Draven
 */
public class CD extends Publicacion{
    public double duracionminutos;

    public CD(double duracionminutos, String titulo, double precio) {
        super(titulo, precio);
        this.duracionminutos = duracionminutos;
    }

    public double getDuracionminutos() {
        return duracionminutos;
    }
    
    
}
