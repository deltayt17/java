/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author Draven
 */
public class Camioneta extends Coche{
    
    public double carga;

    public Camioneta(double carga, double velocidad, double cilindrada, String color, int ruedas) {
        super(velocidad, cilindrada, color, ruedas);
        this.carga = carga;
    }

    public double getCarga() {
        return carga;
    }
    
    
    
}
