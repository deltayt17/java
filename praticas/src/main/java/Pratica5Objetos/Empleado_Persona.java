/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author Draven
 */
public class Empleado_Persona extends Pratica5Objetos.Persona{
    
   //Atributos
    public String jefe;
    
    public Empleado_Persona() {
    }

    public Empleado_Persona (String jefe, String Nombre,String horario,String domicilio) {
        this.jefe = jefe;
        setNombre(Nombre);
        setHorario(horario);
        setDomicilio(domicilio);
    }

    public String getJefe() {
        return jefe;
    }

    public void setJefe(String jefe) {
        this.jefe = jefe;
    }
    public void Cobrar(){
    
    }

    @Override
    public String toString() {
        return "Estudiante_Persona{"+ "El nombre es " + getNombre() + "\n El Domicilio es " + getDomicilio() + "\n  El horario es  " + getHorario()  + " \n SU jefe es " + jefe  + '}';
        
    }  
    
}
