/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author dheid
 */
public class Oficial {
    Operario operario1 = new Operario();
    
    public Oficial(){
        this.operario1.empleado2.setNombre("");
    }

    public Oficial(String nombre  ) {
        this.operario1.empleado2.setNombre(nombre);
    }
    
    @Override
    public String toString(){
        return this.operario1.empleado2.toString()+" -> Operario -> Oficial";
    }
}
