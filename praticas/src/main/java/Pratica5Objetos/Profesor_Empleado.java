/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author Draven
 */
public class Profesor_Empleado extends Pratica5Objetos.Empleado_Persona{
    public String Carrera;

    public Profesor_Empleado() {
    }

    public Profesor_Empleado(String carrera, String jefe, String Nombre, String horario, String domicilio) {
        super(jefe, Nombre, horario, domicilio);
        this.Carrera = carrera;

    }

    public void setCarrera(String carrera) {
        this.Carrera = carrera;
    }

    public String getCarrera() {
        return Carrera;
    }
    public void Enseñar(){
    
    }

    @Override
    public String toString() {
        return "Administrador_Persona{" + "la Carrera es  " + Carrera +"\nEl Jefe es " + jefe +"\nEl Nombre " +nombre+"\nEl horario es "+ horario +"\nEl Domicilio es "+domicilio+ '}';
        
    }
}
