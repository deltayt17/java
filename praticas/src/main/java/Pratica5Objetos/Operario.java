/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author dheid
 */
public class Operario {
    Empleado empleado2= new Empleado();
    
    public Operario(){
        this.empleado2.setNombre("");
    }

    public Operario(String nombre  ) {
        this.empleado2.setNombre(nombre);
    }
    
    @Override
    public String toString(){
        return this.empleado2.toString()+" -> Operario";
    }
}
