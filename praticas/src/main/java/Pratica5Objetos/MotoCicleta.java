/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author Draven
 */
public class MotoCicleta extends Bicicleta{
       public double velocidad, cilindrada;

    public MotoCicleta(double velocidad, double cilindrada, String tipo, String color, int ruedas) {
        super(tipo, color, ruedas);
        this.velocidad = velocidad;
        this.cilindrada = cilindrada;
    }

    public double getVelocidad() {
        return velocidad;
    }

    public double getCilindrada() {
        return cilindrada;
    }

    @Override
    public String toString() {
        return "MotoCicleta{" + "velocidad=" + velocidad + ", cilindrada=" + cilindrada +" Tipo= " + tipo+"color= "+color+"ruedas"+ruedas+ '}';
        
    }
       
    
    
}
