package Pratica5Objetos;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Draven
 */
public class Estudiante_Persona extends Pratica5Objetos.Persona{

    //atributos
    public int Grado;
    public String grupo;
    //constructores
    public Estudiante_Persona() {
    }

    public Estudiante_Persona(int Grado, String grupo, String Nombre,String horario,String domicilio) {
        this.Grado = Grado;
        this.grupo = grupo;
        setNombre(Nombre);
        setHorario(horario);
        setDomicilio(domicilio);
    }
    //metodos
    public String getGrupo() {
        return grupo;
    }

    public int getGrado() {
        return Grado;
    }

    public void setGrado(int Grado) {
        this.Grado = Grado;
    }
    public void Estudiar(){
    
    }
    //metodoSobreEscritura
    @Override
    public String toString() {
        return "Estudiante_Persona{"+ "El nombre es " + getNombre() + "\n El Domicilio es " + getDomicilio() + "\n  El horario es  " + getHorario()  + " \nGrado= " + Grado + ", grupo=  " + grupo + '}';
        
    }  
}
