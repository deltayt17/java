/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author Draven
 */
public class Persona {
    //atributos
    public String nombre;
    
    public String domicilio;
    
    public String horario;
    
    //Constructores
    public Persona() {
    }
    
    public Persona(String nombre, String dominicilio, String horario){
        this.nombre = nombre;
        this.domicilio = dominicilio;
        this.horario = horario;
    
    }
    //metodos
    public String getNombre(){
        return nombre;
    }
    public void setNombre(String nombre){
        this.nombre= nombre;
    }
    public String getDomicilio(){
        return domicilio;
    }
    public void setDomicilio(String domicilio){
        this.domicilio = domicilio;
    }
    
    public String getHorario(){
        return horario;
    }
    public void setHorario(String horario){
        this.horario = horario;
    }
    public void Asistir(){
        
    }

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", domicilio=" + domicilio + ", horario=" + horario + '}';
    }
    
    
    
}
