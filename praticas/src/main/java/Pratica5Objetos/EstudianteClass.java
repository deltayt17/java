/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author Draven
 */
public class EstudianteClass {
    //atributos
    private double Matemática ;
    private double  Sociales;
    private double Informática;
    private double Deporte ;
    private double Historia;
    private double Naturales;
    private double Promedio;

   
    
    
    //constructores
    public EstudianteClass(){
        this.Matemática = Double.NaN;
        this.Sociales = Double.NaN;
        this.Informática = Double.NaN;
        this.Deporte = Double.NaN;
        this.Historia = Double.NaN;
        this.Naturales = Double.NaN;
        this.Promedio = Double.NaN;
        
    }
    public EstudianteClass(Double matematica, Double Sociales, Double Informatica, Double Deportes,Double Historia, Double Naturales ){
        this();
        this.Matemática= matematica;
        this.Sociales = Sociales;
        this.Informática = Informatica;
        this.Deporte = Deportes;
        this.Historia = Historia;
        this.Naturales = Naturales;
    
    }
    public void calcularPromedio(){
        Promedio =(Matemática + Sociales + Informática + Deporte + Historia + Naturales) / 6;
    }
    @Override
    public String toString() {
        return String.format("Matematica : %s\nSociales: %s\nInformatica: %s\nDeporte: %s\nDeporte: %s\nNaturales: %s\nPromedio: %s",
            Matemática,
            Sociales,
            Informática,
            Deporte,
            Historia,
            Naturales,
            Math.round(Promedio)
        );
    }
    //metodos
    public void  setMatematica (Double Matematica){
        this.Matemática = Matematica;
        
    }
    public void  setSociales (Double Sociales){
        this.Sociales = Sociales;
        
    }
    public void  setInformatica (Double Informatica){
        this.Informática = Informatica;
        
    }
    public void  setDeporte (Double Deporte){
        this.Deporte = Deporte;
        
    }
    public void  setHistoria (Double Historia){
        this.Historia = Historia;
        
    }
    public void  setNaturales (Double Naturales){
        this.Naturales = Naturales;
        
    }
    public Double getPromedio(){
        return Math.round(Promedio - 0)/1d;
    }
    
}
