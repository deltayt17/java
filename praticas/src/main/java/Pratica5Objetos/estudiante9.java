/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author Draven
 */
public class estudiante9 {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        // TODO code application logic here
        java.util.Scanner read = new java.util.Scanner(System.in);
        System.out.println("Cantidad de Estudiantes: ");
        EstudianteClass [] estudiantes = new EstudianteClass[read.nextInt()];
        for (int i = 0; i < estudiantes.length; i++) {
            System.out.println("Notas de Matematicas");
            double mat = read.nextDouble();
            System.out.println("Notas de Historia");
            double his = read.nextDouble();
            System.out.println("Notas de Deporte");
            double dep = read.nextDouble();
            System.out.println("Notas de Informatica");
            double inf = read.nextDouble();  
            System.out.println("Notas de Naturales");
            double nat = read.nextDouble();
            System.out.println("Notas de Sociales");
            double soc = read.nextDouble();
            estudiantes [i]= new EstudianteClass(mat, soc, nat, dep, his, nat);
            estudiantes [i].calcularPromedio();
            System.out.println();  
        }
        System.out.println("\nREsultado");
        for (int i = 0; i < estudiantes.length; i++)
        {
            System.out.println("Estudiante #" + (i+1));
            System.out.println(estudiantes[i].toString());
            System.out.println((estudiantes[i].getPromedio() >= 70) ? "\n----aprobado----" : "\n -- Reprobado --\n");
        }
    }
}
