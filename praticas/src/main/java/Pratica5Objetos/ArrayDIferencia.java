/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

import java.util.*;

/**
 *
 * @author Draven
 */
public class ArrayDIferencia {

    /**
     * @param args the command line arguments
     */
    //creando metodo de diferencia de conjuntos
    public static Set diferencia(Set a, Set b) {
        Set res = new TreeSet(a);
        res.removeAll(b);
        return res;
    }
    //creando la union de conjuntos
    public static Set union(Set a, Set b) {
        Set res = new TreeSet(a);
        res.addAll(b);
        return res;
    }
    
    public static void main(String args[]) {
        // TODO code application logic here
        
        // aqui creamos los arraylist a y b
        
        List<Integer> proA = Arrays.asList(12,23,44,65,76,888,66,54,43,22,77,56,34);
        List<Integer> proB = Arrays.asList(10,27,47,65,76,889,66,56,48,22,77,59,34);
        
    
        
        //GENERAMOS TREE-SETS DE LOS ARRAYS DE STRINGS        
        Set<Integer> a = new  TreeSet<>(proA);
       Set<Integer> b = new TreeSet<>(proB);
        
        System.out.println("\nCONJUNTOS:");
        System.out.println("Conjunto A: " + a);
        System.out.println("Conjunto B: " + b);
        
        Set c = union(a, b);
        System.out.println("Union: " + c);
        
        c = diferencia(a, b);               
        System.out.println("Diferencia: " + c);
    }
}
