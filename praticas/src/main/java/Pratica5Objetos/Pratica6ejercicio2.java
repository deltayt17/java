/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author Draven
 */
public class Pratica6ejercicio2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        // TODO code application logic here
        Persona persona = new Persona("Juan", "Sabana Perida", "Noche");
        Estudiante_Persona es = new Estudiante_Persona(8, "a","julio","noche","los mina");
        Empleado_Persona em = new Empleado_Persona("Heidy","ronny", "tarde", "Villa juana");
        Administrador_Persona ap = new Administrador_Persona("Auxiliar", "Ronny", "Eudy", "Tarde", "Sabana Perdida");
        Profesor_Empleado pe = new Profesor_Empleado("Licenciado en programacion", "Ronny", "Juan", "Tarde", "Mirador");
        System.out.println("------CLasePersona------");
        System.out.println("\n" + persona.toString()+ "\n");
        System.out.println("\n ------ClaseEstudiante------\n");
        System.out.println(es.toString()+ "\n");
        System.out.println("\n ------CLaseEmpleado------\n");
        System.out.println(em.toString()+ "\n");    
        System.out.println("\n ------CLaseAdministrador------\n");
        System.out.println(ap.toString());
        System.out.println("\n ------CLaseProfesor------\n");
        System.out.println(pe.toString());
        
    }
}
