/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author Draven
 */
public class Bicicleta extends Vehichulo{
    public String tipo;

    public Bicicleta(String tipo, String color, int ruedas) {
        super(color, ruedas);
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }
    
    
}
