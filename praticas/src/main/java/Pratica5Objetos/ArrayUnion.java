/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pratica5Objetos;

/**
 *
 * @author Draven
 */
public class ArrayUnion {

    /**
     * @param args the command line arguments
     */
    public static int [] ConcatenarArray(int [] o1, int [] o2)
	{
		int[] ret = new int[o1.length + o2.length];
 
		System.arraycopy(o1, 0, ret, 0, o1.length);
		System.arraycopy(o2, 0, ret, o1.length, o2.length);
 
		return ret;
	}
    public static void main(String args[]) {
        // TODO code application logic here
        int[] A = {12,23,44,65,76,888,66,54,43,22,77,56,34};
        int[] B = {12,23,44,65,76,888,66,54,43,22,77,56,34};
        int[] C = ConcatenarArray(A,B);
        
        for (int i = 0; i < C.length; i++) {
            System.out.println(C[i]);
        }
    }
}
